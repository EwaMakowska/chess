﻿// jest.config.js
process.env.VUE_CLI_BABEL_TARGET_NODE = true;
process.env.VUE_CLI_BABEL_TRANSPILE_MODULES = true;

module.exports = {
    verbose: true,
    moduleFileExtensions: ['js'],
    transformIgnorePatterns: [
        "node_modules/(?!(babel-jest|jest-vue-preprocessor)/)"
    ],
    transform: {
      '.+\\.(css|styl|less|sass|scss|png|jpg|ttf|woff|woff2)$':
        'jest-transform-stub',
      '^.+\\.(js)?$': 'babel-jest'
    },
    moduleNameMapper: {
      '^@/(.*)$': '<rootDir>/src/$1'
    },
    // testMatch: [
    //     '**/tests/unit/**/*.spec.(js|jsx|ts|tsx)|**/__tests__/*.(js|jsx|ts|tsx)'
    //   ],
    transformIgnorePatterns: ['<rootDir>/node_modules/']
  };