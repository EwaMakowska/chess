﻿import $ from "jquery";
import { ColorsFigures } from "../../utils/ColorsFigures";
import { ChessboardIDs } from "../../utils/ChessboardIDs";
import { InitialPosition } from "../../utils/InitialPosition"


import Square from "./Square";
import King from "../figures/figures/King";
import Queen from "../figures/figures/Queen";
import Pawn from "../figures/figures/Pawn";
import Bishop from "../figures/figures/Bishop";
import Rook from "../figures/figures/Rook";
import Knight from "../figures/figures/Knight";

export default class Chessboard {
    createBord(){ 
        this.squares = {};
        var self = this;

        ChessboardIDs.LETTERS.map(function(letter){
            ChessboardIDs.NUMBERS.map(function(number){
                var key = letter+number;
                var square = new Square(key);
                self.squares[key] = square;                  
            })
        })
        this.setFigures();
    }

    setFigures(){
        var self = this;
        ColorsFigures.map(function(color){
            var king = new King(color, 0);
            king.position = InitialPosition["KING"][color.NAME];
            self.squares[king.position].figure = king;
            var queen =  new Queen(color, 0);
            queen.position = InitialPosition["QUEEN"][color.NAME];
            self.squares[queen.position].figure = queen; 

            for(let i = 0; i < 2; i++){  
                var bishop = new Bishop(color, i);
                bishop.position = InitialPosition["BISHOP"][color.NAME][i];
                self.squares[bishop.position].figure = bishop;
                var rook = new Rook(color, i);
                rook.position = InitialPosition["ROOK"][color.NAME][i];
                self.squares[rook.position].figure = rook;
                var knight = new Knight(color, i);
                knight.position = InitialPosition["KNIGHT"][color.NAME][i];
                self.squares[knight.position].figure = knight;
            }

            for(let i = 0; i < 8; i++){
                var pawn = new Pawn(color, i)
                pawn.position = InitialPosition["PAWN"][color.NAME][i];
                self.squares[pawn.position].figure = pawn; 
            }
        })
    }

    get squares(){
        return this._squares;
    }

    set squares(squares){
        this._squares = squares;
    }
}