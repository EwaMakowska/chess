﻿import { MarkSquareType } from '../../utils/MarkSquareType';

export default class Square {
    constructor(position){
        this.position = position;
        this.mark = MarkSquareType.NONE;
    }

    get figure(){
        return (this._figure == undefined)?"":this._figure;
    }

    set figure(figure){
        this._figure = figure;
    }
    
    // set figureLabel(figureLabel){
    //     this._figureLabel = figureLabel;
    // }

    // get figureLabel(){
    //     return (this._figure == undefined)?"":this._figure.label;
    // }

    set mark(mark){
        this._mark = mark;
    }

    get mark(){
        return this._mark;
    }

    get occupied(){
        if(this.figure != ""){
            return true
        } else {
            return false
        }
    }
}
