﻿export default class Chesspiece {
    constructor(color, lineIndex){
        this.color = color;
        this.lineIndex = lineIndex;
    }

    set position(position){
        this._position = position;
    }

    get position(){
        return this._position;
    }

    set inGame(inGame){
        this._inGame = inGame;
    }

    get inGame(){
        return inGame;
    }

    set distance(distance){
        this._distance = distance;
    }

    get distance(){      
        return 8;
    }

    set attackMoves(attackMoves){
        this._attackMoves = attackMoves;
    }

    get attackMoves(){
        return this._attackMoves;
    }

    set initialPosition(initialPosition){
        this._initialPosition = initialPosition;
    }

    get initialPosition(){
        return this._initialPosition;
    }
}