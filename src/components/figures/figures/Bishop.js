﻿
import { preapareMovesFigures } from '../../../utils/FiguresMoves';

import Figure from '../Figure'

export default class Bishop extends Figure{
    constructor(color, lineIndex){
        super(color, lineIndex);        
        this.name = "Bishop";
        this.moves = [
            preapareMovesFigures.CROSS_UP_LEFT,
            preapareMovesFigures.CROSS_UP_RIGHT,
            preapareMovesFigures.CROSS_DOWN_LEFT,
            preapareMovesFigures.CROSS_DOWN_RIGHT
        ]
        this.position = this.setInitialPosition();
        this.label = "B";
    }

    setInitialPosition(){
        var initialPositionBlack = ["A3", "A6"];
        var initialPositionWhite = ["H3", "H6"];
        var position = (this.color.NAME == "black")?initialPositionBlack[this.lineIndex]:initialPositionWhite[this.lineIndex]; 
        return position;
    }
}