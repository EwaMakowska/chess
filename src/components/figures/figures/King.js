﻿
import { preapareMovesFigures } from '../../../utils/FiguresMoves'

import Figure from '../Figure'

export default class King extends Figure{
    constructor(color){
        super(color);        
        this.name = "King";
        this.moves = [
            preapareMovesFigures.STRIGHT_UP,
            preapareMovesFigures.STRIGHT_DOWN,
            preapareMovesFigures.STRIGHT_LEFT,
            preapareMovesFigures.STRIGHT_RIGHT,
        ]
        this.label = "K";        
    }

    set distance(distance){
        this._distance = distance;
    }

    get distance(){
        return 1
    }
}

