﻿import { preapareMovesFigures } from '../../../utils/FiguresMoves';

import Figure from '../Figure'

export default class Knight extends Figure{
    constructor(color, lineIndex){
        super(color, lineIndex);        
        this.name = "Knight";
        this.moves = [
            preapareMovesFigures.KNIGHT_UP_LEFT,
            preapareMovesFigures.KNIGHT_UP_RIGHT,
            preapareMovesFigures.KNIGHT_LEFT_DOWN,
            preapareMovesFigures.KNIGHT_RIGHT_DOWN,
            preapareMovesFigures.KNIGHT_DOWN_LEFT,
            preapareMovesFigures.KNIGHT_DOWN_RIGHT,
            preapareMovesFigures.KNIGHT_LEFT_UP,
            preapareMovesFigures.KNIGNT_RIGHT_UP
        ]
        this.position = this.setInitialPosition();
        this.label = "Kn";        
    }

    setInitialPosition(){
        var initialPositionBlack = ["A2", "A7"];
        var initialPositionWhite = ["H2", "H7"];
        var position = (this.color.NAME == "black")?initialPositionBlack[this.lineIndex]:initialPositionWhite[this.lineIndex]; 
        return position;
    }

    set distance(distance){
        this._distance = distance;
    }

    get distance(){
        return 1
    }
}