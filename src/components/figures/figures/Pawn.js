﻿import { preapareMovesFigures } from '../../../utils/FiguresMoves';

import Figure from '../Figure'

export default class Pawn extends Figure{
    constructor(color, lineIndex){
        super(color, lineIndex);        
        this.name = "Pawn";
        this.position = this.setInitialPosition();
        this.moves = [];
        this.setDirection();
        this.attackMoves = [
            preapareMovesFigures.CROSS_UP_LEFT,
            preapareMovesFigures.CROSS_UP_RIGHT
        ]
        this.label = "P";
    }

    setInitialPosition(){
        var initialPositionBlack = ["B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8"];
        var initialPositionWhite = ["G1", "G2", "G3", "G4", "G5", "G6", "G7", "G8"];
        var position = (this.color.NAME == "black")?initialPositionBlack[this.lineIndex]:initialPositionWhite[this.lineIndex]; 
        this.initialPosition = position;
        return position;
    }

    setDirection(){
        if(this.color.NAME == "black"){
            this.moves.push(preapareMovesFigures.STRIGHT_UP)
        }
        if(this.color.NAME == "white"){
            this.moves.push(preapareMovesFigures.STRIGHT_DOWN)
        }
    }

    set distance(distance){
        this._distance = distance;
    }

    get distance(){
        var distance = 1;
        if(this.position == this.initialPosition){
            distance = 2;
        }
        return distance;
    }
}