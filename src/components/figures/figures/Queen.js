﻿import { preapareMovesFigures } from '../../../utils/FiguresMoves'

import Figure from '../Figure'

export default class Queen extends Figure{
    constructor(color){
        super(color);        
        this.name = "Queen";
        this.moves = [
            preapareMovesFigures.STRIGHT_UP,
            preapareMovesFigures.STRIGHT_DOWN,
            preapareMovesFigures.STRIGHT_LEFT,
            preapareMovesFigures.STRIGHT_RIGHT,
            preapareMovesFigures.CROSS_UP_LEFT,
            preapareMovesFigures.CROSS_UP_RIGHT,
            preapareMovesFigures.CROSS_DOWN_LEFT,
            preapareMovesFigures.CROSS_DOWN_RIGHT
        ]
        this.label = "Q";        
    }
}