﻿import { preapareMovesFigures } from '../../../utils/FiguresMoves';

import Figure from '../Figure';

export default class Rook extends Figure{
    constructor(color, lineIndex){
        super(color, lineIndex);        
        this.name = "Rook";
        this.moves = [
            preapareMovesFigures.STRIGHT_UP,
            preapareMovesFigures.STRIGHT_DOWN,
            preapareMovesFigures.STRIGHT_LEFT,
            preapareMovesFigures.STRIGHT_RIGHT,
        ];
        this.position = this.setInitialPosition();        
        this.label = "R";
    }

    setInitialPosition(){
        var initialPositionBlack = ["A1", "A8"];
        var initialPositionWhite = ["H1", "H8"];
        var position = (this.color.NAME == "black")?initialPositionBlack[this.lineIndex]:initialPositionWhite[this.lineIndex]; 
        return position;
    }
}