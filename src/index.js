﻿import $ from "jquery";
import { MarkSquareType } from './utils/MarkSquareType';


import Chessbord from './components/chessboard/Chessboard';
import View from './view/View';
import Moves from './moves/Moves';

jQuery(function( $ ){

    var container = $("#container");
    var chessboard;
    var view;
    var squares;
    var moves;

    var chess = function(){
        chessboard = new Chessbord();
        chessboard.createBord();
        squares = chessboard.squares;
        view = new View(container, squares);        
        view.createBord()        
        moves = new Moves(squares)
    }()

    $(container).on("click", '.figure', function(){      
            var startPosition = $(this).attr("position-attr")   
            moves.preapare(startPosition);
            view.refreshBoard(moves.greenSquares);
    })

    // $(container).on("click", ".square.green", function(e){
    //         var toPosition = $(this).attr("position-attr");
    //         moves.go(toPosition);      
    //         view.refreshView();              
    // })
})