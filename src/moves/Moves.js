﻿import { MarkSquareType } from "../utils/MarkSquareType";
import { ChessboardIDs } from "../utils/ChessboardIDs";

export default class Moves {
    constructor(squares){
        this.squares = squares;
        this.letters = ChessboardIDs.LETTERS;
        this.numbers = ChessboardIDs.NUMBERS;
        this.greenSquares = [];
        console.log("squares", this.squares)
    }

    preapare(startSquar){
        this.selfFigure = this.squares[startSquar].figure;

        this.selfLetter = startSquar[0];
        this.selfNumber = startSquar[1];

        this.markSquares();
    }

    markSquares(){
        this.greenSquares = [];
        for(let move in this.selfFigure.moves){
            var colIndex = ChessboardIDs.LETTERS.indexOf(this.selfLetter);
            var rowIndex = ChessboardIDs.NUMBERS.indexOf(this.selfNumber);
       
            for(let i = 0; i < this.selfFigure.distance; i++){
                var nextLetter;
                var nextNumber;
                colIndex += this.selfFigure.moves[move].col;
                rowIndex += this.selfFigure.moves[move].row;
                nextLetter = ChessboardIDs.LETTERS[colIndex];
                nextNumber = ChessboardIDs.NUMBERS[rowIndex];
                if(nextLetter == undefined || nextNumber == undefined){
                    continue
                }
                var nextPosition = nextLetter+nextNumber;
                this.greenSquares.push(nextPosition);
                if(this.detectCollision(this.squares[nextPosition]) == false){
                    break
                }   
            }
        }
    }

    detectCollision(square){
        if(square.figure != undefined && square.figure != "" && square != null){   
            this.detectEnemy(square);
            return false;
        } else {
            square.mark = MarkSquareType.AVAILABLE;
        }
        
    }  

    detectEnemy(square){
        if(square.figure.color != this.selfFigure.color){
            square.squareMark = MarkSquareType.ATTACK;
        }
    }

    go(to){
        this.squares[this.selfFigure.position].figure = undefined;
        this.selfFigure.position = to;
        this.squares[to].figure = this.selfFigure;     
    }    

    get selfFigure(){
        return this._selfFigure;
    }
      
    set selfFigure(selfFigure){
        this._selfFigure = selfFigure;
    }

    get distance(){
        return this._distance;
    }
      
    set distance(distance){
        this._distance = distance;
    }

    get selfLetter(){
        return this._selfLetter;
    }

    set selfLetter(selfLetter){
        this._selfLetter = selfLetter;
    }

    get selfNumber(){
        return this._selfNumber;
    }

    set selfNumber(selfNumber){
        this._selfNumber = selfNumber;
    }

    get greenSquares(){
        return this._greenSquares;
    }

    set greenSquares(greenSquares){
        this._greenSquares = greenSquares;
    }
}