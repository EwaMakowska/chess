﻿export const ColorsFigures = [
    {
        NAME: "black",
        HEX: "#616161"
    },
    {
        NAME: "white",
        HEX: "#B0BEC5"
    }
]