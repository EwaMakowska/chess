﻿export const preapareMovesFigures = {
    STRIGHT_UP: {
       col : 1,
       row : 0
    },
    STRIGHT_DOWN: {
        col : -1,
        row : 0
    },
    STRIGHT_LEFT: {
        col : 0,
        row : -1
    },
    STRIGHT_RIGHT:  {
        col : 0,
        row : 1
    },
    CROSS_UP_LEFT: {
        col : 1,
        row : -1
    },
    CROSS_UP_RIGHT:  {
        col : 1,
        row : 1
    },
    CROSS_DOWN_LEFT:  {
        col : -1,
        row : -1
    },
    CROSS_DOWN_RIGHT:  {
        col : -1,
        row : 1
    },
    KNIGHT_LEFT_UP:  {
        col : 1,
        row : -2
    },
    KNIGNT_RIGHT_UP:  {
        col : 1,
        row : 2
    },
    KNIGHT_LEFT_DOWN:  {
        col : -1,
        row : -2
    },
    KNIGHT_RIGHT_DOWN:  {
        col : 1,
        row : 2
    },
    KNIGHT_UP_LEFT:  {
        col : 2,
        row : -1
    },
    KNIGHT_UP_RIGHT:  {
        col : 2,
        row : 1
    },
    KNIGHT_DOWN_LEFT:  {
        col : -2,
        row : -1
    },
    KNIGHT_DOWN_RIGHT:  {
        col : -2,
        row : 1
    },
}