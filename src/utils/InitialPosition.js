﻿export const InitialPosition = {
    KING: {
        black: "H4",
        white: "A4"
    },
    QUEEN: {
        black: "A5",
        white: "H5"
    },
    BISHOP: {
        black: ["A3", "A6"],
        white: ["H3", "H6"]
    },
    KNIGHT: {
        black: ["A2", "A7"],
        white: ["H2", "H7"]
    },
    ROOK: {
        black: ["A1", "A8"],
        white: ["H1", "H8"]
    },
    PAWN: {
        black: ["B1", "B2", "B3", "B4", "B5", "B6", "B7", "B8"],
        white: ["G1", "G2", "G3", "G4", "G5", "G6", "G7", "G8"],
    }
}