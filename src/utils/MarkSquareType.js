﻿export const MarkSquareType = {
    NONE: "",
    AVAILABLE: "green",
    ATTACK: "red",
    DANGEROUS: "yellow"
}