﻿import { ChessboardIDs } from "../utils/ChessboardIDs";

export default class Chessboard {
    constructor(container, squares){
        this.container = $(container);
        this.squares = squares;
        this.container.html("<div id='chessboard'><div id='col'></div><div id='row'></div><div class='chessboard'></div></div>");
    }

    createBord(){
        var chessboard = this.container.find("#chessboard");
        var row = chessboard.find("#row");
        var col = chessboard.find("#col");
        for(let i in ChessboardIDs.NUMBERS){
            col.append("<div>"+ChessboardIDs.NUMBERS[i]+"</div");
        }
        for(let i in ChessboardIDs.LETTERS){
            row.append("<div>"+ChessboardIDs.LETTERS[i]+"</div");
        }
        for(let i in this.squares){
            var html = "<div class='square "+this.squares[i].mark+"' position-attr='"+this.squares[i].position+"'>"+this.squares[i].figureLabel+"</div>";                
            chessboard.find(".chessboard").append(html);
        }

        this.setFigures();
    }

    setFigures(){
        for(let i in this.squares){
            if(this.squares[i].figure.name != undefined){
                var position = this.squares[i].figure.position;
                var color = this.squares[i].figure.color.HEX;
                var label = this.squares[i].figure.label;
                var html = "<span class='figure' position-attr='"+this.squares[i].figure.position+"' style='color:"+color+"'>"+label+"</span>";
                var square = this.container.find(".square[position-attr="+position+"]");
                square.html(html);  
            }
            if(this.squares[i].figure.name == undefined){
                var html = ""
                var square = this.container.find(".square[position-attr="+i+"]");
                square.html(html);
            }
        }
    }

    refreshBoard(markSquare){
        console.log("test2", this.squares)
        for(var i in markSquare){
            this.container.find(".square[position-attr="+markSquare[i]+"]").addClass("green")
        }
    }

    set container(container){
        this._container = container;
    }

    get container(){
        return this._container;
    }

    set squares(squares){
        this._squares = squares;
    }

    get squares(){
        return this._squares;
    }
}