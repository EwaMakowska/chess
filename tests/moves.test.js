﻿import Moves from '../src/moves/PreapareMoves';

var squares = {
    B1: {
      Square: {
          position: "C1",
          squareMark: "",
          figure: "",     
      }
    },
    B2: {
      Square: {
        position: "B2",
        squareMark: "",
        figure: {
            Rook: {
                color: "#616161",
                label: "<span style='color:#616161'>B</span>",
                lineIndex: 0,
                name: "Bishop",
                position: "B2",
                inGame: true       
            }
          }       
      }
    },
}

var moves = new Moves(squares);

//detect collision
test("if collision is detected return true", () => {
  expect(moves.detectCollision("B1")).toBeTruthy;
});

test("if collision isn't detected return false", () => {
  expect(moves.detectCollision("B2")).toBeFalsy;
});

var squares = {
  B1: {
    Square: {
      position: "A2",
      squareMark: "",
      figure: {
          Rook: {
              color: "#B0BEC5",
              label: "<span style='color:#616161'>B</span>",
              lineIndex: 0,
              name: "Bishop",
              position: "A2",
              inGame: true       
          }
        }       
    }
  },
  B2: {
    Square: {
      position: "B2",
      squareMark: "",
      figure: {
          Rook: {
              color: "#616161",
              label: "<span style='color:#616161'>B</span>",
              lineIndex: 0,
              name: "Bishop",
              position: "b2",
              inGame: true       
          }
        }       
    }
  },
}

var moves = new Moves(squares);
var figure1 = squares["B1"].Square.figure.Rook;
var figure2 = squares["B2"].Square.figure.Rook;

var selfFigure = {
  color: "#616161"
}
moves.selfFigure = selfFigure;

//detect enemy
test("if figure have different color return true", () => {
  expect(moves.detectEnemy(figure1)).toBeTruthy;
});

test("if collision isn't detected return false", () => {
  expect(moves.detectEnemy(figure2)).toBeFalsy;
});

