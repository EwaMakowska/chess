﻿const {defaults} = require('jest-config');

module.exports = {
    entry: './src/index.js',
    output: {
        path: `${__dirname}/dist/js`,
        filename: 'chess.js'
    },
    watch: true,
    mode: "development",
    devtool: "source-map",
    module: {
        rules: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                use: {
                    loader: ["babel-loader", "eslint-loader"],
                    options: {
                        presets: [
                            '@babel/preset-env',
                                targets: {
                                    node: 'current',
                                },
                            ]
                    }
                }
            }
        ]
    },
    moduleFileExtensions: [...defaults.moduleFileExtensions, 'js'],
}